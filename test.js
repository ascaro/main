// URL validation for Alphuite (https://github.com/alphuite)
const url = 'https://osu.ppy.sh/b/705378?m=0';
const test = /^https?:\/\/osu.ppy.sh\/b\//.test(url);
// remove the two // below to test it.
//console.log(test);

const mode = '6.113048553466797';
const parsed = parseFloat(mode).toFixed(2);
//console.log(parsed);
