const gulp = require('gulp');
const changed = require('gulp-changed');
const concat = require('gulp-concat');
const uglify = require ('gulp-uglify');
const imagemin = require ('gulp-imagemin');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const minifyCSS = require ('gulp-minify-css');
const sourcemaps = require('gulp-sourcemaps');

const input = __dirname + '/gulp/';

// COMPRESS IMAGES
gulp.task('img', function() {
  const imgSrc = './gulp/img/**/*.{png,gif,jpg,jpeg,svg,ico}', imgDst = './assets/public/img';

  gulp.src(imgSrc)
    .pipe(changed(imgDst))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));
});

// COMPRESS JS
gulp.task('jsHome', function(){
  let jsSrc = ['js/home.js'], jsDst = './assets/public/js';

  jsSrc = jsSrc.map((jsSrc) => input +  jsSrc);

  gulp.src(jsSrc)
    .pipe(concat('home.min.js'))
    .pipe(babel({presets: ['es2015']}))
    .pipe(uglify())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(jsDst));
});

// COMPILE & COMPRESS SASS => CSS
gulp.task('sassHome', function(){
  let sassSrc = ['sass/home.sass'], sassDst = './assets/public/css';

    sassSrc = sassSrc.map((sassSrc) => input +  sassSrc);

    gulp.src(sassSrc)
      .pipe(sass ({outputStyle: 'expanded'}))
      .pipe(minifyCSS())
      .pipe(concat('home.min.css'))
      .pipe(gulp.dest(sassDst));
});

gulp.task('watch', function(){
  gulp.watch('./gulp/sass/home.sass', ['sassHome']);
  gulp.watch('./gulp/sass/_core.sass', ['sassHome']);
  gulp.watch('./gulp/img/**/*', ['img']);
  gulp.watch('./gulp/js/home.js', ['jsHome']);
});

gulp.task('default', ['sassHome','img','jsHome','watch']);
