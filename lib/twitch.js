module.exports = (container) => {
  'use strict';

  const config = container.config;
  const mysql = container.mysql;
  // lib start
  const lib = {};

  const options = {
    options: {
      debuf: true,
      clientId: config.twitch.clientId
    },
    connection: {
      reconnect: true,
      reconnectAttempts: 50,
      reconnectInterval: 30000,
      secure: true,
      timeout: 60000
    },
    identity: {
      username: config.twitch.username,
      password: config.twitch.password
    },
    channels: config.twitch.channels
  };

  lib.util = new (require('tmi.js')).client(options);

  mysql.util.on('dbReady', () => {
    lib.util.connect();
  });
  
  // lib end
  return lib;
};
