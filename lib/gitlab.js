module.exports = (container) => {
  'use strict';

  const config = container.config;

  // NPM Packages
  const http = require('http');
  const createHandler = require('gitlab-webhook-handler');
  const handler = createHandler({path: '/webhook'});

  // lib start
  const lib = {};

  http.createServer((req, res) => {
    handler(req, res, (err){
      res.statusCode = 404
      res.end('no such location')
    });
  }).listen(9090);

  handler.on('push', (event) => {
    console.log('Received a push event for %s to %s',
    event.payload.repository.name,
    event.payload.ref)
  });

  // lib end
  return lib;
};
