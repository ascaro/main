module.exports = (container) => {
  'use strict';

  const config = container.config;

  // lib start
  const lib = {};

  lib.util = new (require('mysql')).createConnection({
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
  });

  lib.util.connect((err) => {
    if(err){
      lib.util.emit('dbError');
    } else {
      lib.util.emit('dbReady');
    }
  });

  // lib end
  return lib;
};
