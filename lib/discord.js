module.exports = (container) => {
  'use strict';

  const config = container.config;
  const mysql = container.mysql;
  // lib start
  const lib = {};

  lib.util = new (require('discord.io')).Client({
    token: config.discord.token,
    autorun: false
  });

  mysql.util.on('dbReady', () => {
    lib.util.connect();
  });
  
  // lib end
  return lib;
};
