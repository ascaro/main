module.exports = (container) => {
  'use strict';

  const config = container.config;
  const mysql = container.mysql;

  // NPM MODULES
  const fs = require('fs');
  const path = require('path');
  const http = require('http');
  const cluster = require('cluster');
  const express = require('express');
  const socketio   = require('socket.io');
  const bodyParser = require('body-parser');
  const cookieParser = require('cookie-parser');

  // CONTAINER MODULES
  const discord = container.discord;

  // lib start
  const lib = {};

  if (cluster.isMaster) {
    const cpuCount = require('os').cpus().length;
    for (let i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
  } else {
    // Cluster start
    lib.express = express();
    lib.util = http.createServer(lib.express);

    // cookies & sessions
    lib.express.use(cookieParser());

    lib.express.use((req, res, next) => {
        const cookie = req.cookies.cookieName;
        if (cookie === undefined){
            let randomNumber = Math.random().toString();
            randomNumber = randomNumber.substring(2,randomNumber.length);
            res.cookie('sdw6w4gse3', randomNumber, { expire: new Date(), httpOnly: true , secure: true});
        }
        next();
    });

    // EXPRESS SETUP
    lib.express.set('views', `./assets/views`);
    lib.express.set('view engine', 'pug');
    lib.express.use(express.static(`./assets/public`));
    lib.express.use(bodyParser.json());
    lib.express.use(bodyParser.urlencoded({ extended: false }));

    // HOME
    lib.express.get('/', (req, res) => res.render('home'));

    // ERROR Handling
    lib.express.get('*', (req, res) => res.render('error'));

    // SOCKETS
    lib.io = socketio.listen(lib.util);

    lib.io.on('connection', (socket) => {
        discord.util.createInvite({
          channelID: '228133331231965184',
          max_users: '1',
          max_age: '120'
        }, (error, response) => {
            if(!error){
              const invite = response.code;
              socket.emit('discord', {invite: invite});
            }
        });
    });

    mysql.util.on('dbReady', () => {
      lib.util.listen(config.web.port);
    });
  // Cluster end
  }
  // lib end
  return lib;
};
