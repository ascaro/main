(function(){
"use strict";

// container start
const container = {};

  // config
  container.config = require('./assets/config.json');
  const config = container.config;

  // util
  //container.util = require('./lib/util.js')(container);

  // mysql
  container.mysql = require('./lib/mysql.js')(container);

  // twitch
  container.twitch = require('./lib/twitch.js')(container);

  // discord
  container.discord = require('./lib/discord.js')(container);

  // web
  container.web = require('./lib/web.js')(container);

  // gitlab
  container.gitlab = require('./lib/gitlab.js')(container);

// container end
}());
